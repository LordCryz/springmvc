package com.softtek.academy.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sofftek.academy.spring.beans.Person;
import com.softtek.academy.spring.dao.PersonDao;

@Controller
public class PersonController {

	@Autowired
	PersonDao personDao;

	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView("person", "command", new Person());
	}

	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("SpringWeb") Person person, ModelMap model) {
		model.addAttribute("name", person.getName());
		model.addAttribute("age", person.getAge());
		model.addAttribute("id", person.getId());
		personDao.savePerson(person);
		return "viewPerson";
	}

	@RequestMapping("/viewPerson")
	public String viewPerson(Model m) {
		List<Person> list = personDao.getStudents();
		m.addAttribute("list", list);
		return "viewPerson";
	}

	/*
	 * It displays object data into form for the given id. The @PathVariable puts
	 * URL data into variable.
	 */
	@RequestMapping(value = "/editPerson/{id}")
	public String edit(@PathVariable int id, Model m) {
		Person person = personDao.getStudentById(id);
		m.addAttribute("command", person);
		return "editPersonForm";
	}

	/* It updates model object. */
	@RequestMapping(value = "/editsave", method = RequestMethod.POST)
	public String editsave(@ModelAttribute("person") Person person) {
		personDao.update(person);
		return "redirect:/viewPerson";
	}

	/* It deletes record for the given id in URL and redirects to /viewemp */
	@RequestMapping(value = "/deletePerson/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable int id) {
		personDao.delete(id);
		return "redirect:/viewPerson";
	}

}
